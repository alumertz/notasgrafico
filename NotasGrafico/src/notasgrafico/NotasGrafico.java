package notasgrafico;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class NotasGrafico extends Application {
    
    public static CadastrarAlunoController controllerCadastrar;
    public static MenuController controllerMenu;
    public static CalcularMediaController controllerMedia;
    private static Stage stage;
    
    public static Stage getStage() {
        return stage;
    }
    
    
    public static void trocaTelaArray(String tela, ArrayList<Double> avaliacoes, int aluno){
        Parent root = null;
        try{
            FXMLLoader loader = new FXMLLoader(NotasGrafico.class.getResource(tela));
            root = loader.load();
            Scene scene = new Scene(root);
            switch (tela){
                case "Menu.fxml":
                    controllerMenu = (MenuController)loader.getController();
                    controllerMenu.setAvaliacoes(avaliacoes);
                    break;
                
                case "CalcularMedia.fxml":
                    controllerMedia = (CalcularMediaController)loader.getController();
                    controllerMedia.setAvaliacoes(avaliacoes);
                    controllerMedia.setAluno(aluno);
                    break;
            }
                        
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML " + e);
        }        
    }
    
    public static void trocaTelaAvaliacoes(String tela, ArrayList<Aluno> vetAluno , int aluno, ArrayList<Double> avaliacoes){
        Parent root = null;
        try{
            FXMLLoader loader = new FXMLLoader(NotasGrafico.class.getResource(tela));
            root = loader.load();
            Scene scene = new Scene(root);
            
            switch (tela){                
               case "Menu.fxml":
                    controllerMenu = (MenuController)loader.getController();
                    controllerMenu.setAvaliacoes(avaliacoes);
                    controllerMenu.setVetAlunos(vetAluno, aluno);
                    break;
                case "CadastrarAluno.fxml":
                    controllerCadastrar = (CadastrarAlunoController)loader.getController();
                    controllerCadastrar.setVetAlunos(vetAluno);
                    controllerCadastrar.setAvaliacoes(avaliacoes);
                    break;
                case "CalcularMedia.fxml":
                    controllerMedia = (CalcularMediaController)loader.getController();
                    controllerMedia.setVetAlunos(vetAluno,aluno);
                    controllerMedia.setAvaliacoes(avaliacoes);
                    break;
            }
                       
            stage.setScene(scene);
            stage.show();
        }
        catch(Exception e){
            System.out.println("Verificar arquivo FXML " + e);
        }        
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        
        Scene scene = new Scene(root);
        NotasGrafico.stage = stage;
        
        stage.setScene(scene);
        stage.show();
        
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
