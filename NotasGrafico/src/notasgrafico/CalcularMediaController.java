package notasgrafico;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class CalcularMediaController implements Initializable {

    int quant = 0, aluno, qAvaliacaoAnterior;
    ArrayList <Label> labels = new ArrayList<>();
    ArrayList <TextField> notas = new ArrayList<>();
    ArrayList <TextField> pesos = new ArrayList<>();
    ArrayList <Double> avaliacoes = new ArrayList<>();
    ArrayList<Aluno> vetAluno = new ArrayList<>();
    
    @FXML
    private AnchorPane tela;
    
    @FXML
    private Label aviso,harmonica;
    @FXML
    private TextField peso1,nota1, trimestre;
    
    @FXML
    private void salvarAluno(){
        double nHarmonica = calcularMedia();
        if (nHarmonica!=-1 && aluno!=-1){
            int tri = Integer.parseInt(trimestre.getText());
            if (tri<1 || tri>3){
                aviso.setText("Você não inseriu um trimestre válido!");
            }
            else{
                System.out.println("aluno-1 em calcMedia "+ (aluno-1));
                vetAluno.get(aluno-1).notaTrimestre(tri-1, nHarmonica);
                NotasGrafico.trocaTelaAvaliacoes("Menu.fxml", vetAluno,aluno,avaliacoes);
            }
        }
        else{
            aviso.setText("Você não criou um aluno!");
        }
    }
    @FXML
    private double calcularMedia(){
        double nota = 0, nHarmonica=0, somaPesos=0;
        double[] notas1 = new double[quant+1];
        DecimalFormat df = new DecimalFormat("#.00");
        avaliacoes.clear();
                
        notas1[0] = Double.parseDouble(nota1.getText());
        avaliacoes.add (Double.parseDouble(peso1.getText()));
        somaPesos += avaliacoes.get(0);
        nota+= (double) avaliacoes.get(0)/ notas1[0];
        for (int x=0;x<quant && x>=0;x++){
            avaliacoes.add (Double.parseDouble(pesos.get(x).getText()));
            notas1[x+1] = Double.parseDouble(notas.get(x).getText());
            somaPesos += Double.parseDouble(pesos.get(x).getText());
            nota+= (double) avaliacoes.get(x+1)/ notas1[x+1];
        }
        if (somaPesos!=10){
            aviso.setText("A soma dos pesos deve ser igual a 10!");
            avaliacoes.clear();
            return -1;
        }
        else{
            nHarmonica = (10/nota);
            harmonica.setText("Média harmônica: "+ df.format(nHarmonica));
            return nHarmonica;
        }
    }
    @FXML
    public void setVetAlunos (ArrayList<Aluno> vetAlunos, int alunoManipulado){
        vetAluno = vetAlunos;
        aluno = alunoManipulado;
    }
    public void setAvaliacoes(ArrayList<Double> vetAvaliacoes){
        avaliacoes = vetAvaliacoes;
        qAvaliacaoAnterior = avaliacoes.size();
    }
    public void setAluno (int nAluno){
        aluno = nAluno;
    }
    @FXML
    private void adicionar(){
        aviso.setText (" ");
        int posY =126 ;
        for (int x =0; x<quant+1;x++){
            posY +=40;
        }
        
        Label lb = new Label("Avaliação "+ (quant+2)+":");
        lb.setLayoutX(74);
        lb.setLayoutY(posY);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        labels.add(lb) ;
        
        TextField peso = new TextField();        
        peso.setLayoutX(160);
        peso.setLayoutY(posY);
        peso.setPromptText("Insira o peso");
        peso.setMaxWidth(85);
        peso.setId("peso"+(quant+2));
        pesos.add(peso);
        
        TextField notaTX = new TextField();    
        notaTX.setLayoutX(258);
        notaTX.setLayoutY(posY);
        notaTX.setPromptText("Insira a nota");
        notaTX.setMaxWidth(85);
        notaTX.setId("nota"+(quant+2));
        notas.add(notaTX);
            
        tela.getChildren().addAll (lb,peso,notaTX);
        quant ++;
    }
    @FXML
    private void remover(){
        
        aviso.setText ("");
        if (quant>=1){
            quant--;

            tela.getChildren().remove (labels.get(quant));
            tela.getChildren().remove (notas.get(quant));
            tela.getChildren().remove (pesos.get(quant));
            labels.remove(quant);
            notas.remove (quant);
            pesos.remove(quant);
        }
        else{
            aviso.setText("Você não pode ter menos de uma avaliação!");
        }
        
    }
    @FXML
    private void usarAnterior(){
        int quantAnterior = quant;
        if (labels.size()>0){
            for (int x = 0 ; x<=quantAnterior;x++){
                remover();
            }
        }
        if(avaliacoes.size() >=1){
            peso1.setText(Double.toString(avaliacoes.get(0)));
            if (avaliacoes.size() >1){
                for (int x=1; x<avaliacoes.size(); x++){
                    adicionar();
                    pesos.get(x-1).setText(Double.toString(avaliacoes.get(x)));
                }
            }
        }
            else{
            aviso.setText("Você não configurou nenhuma avaliação antes!");
        }
    }
    @FXML
    private void voltar(){
        if (vetAluno.size()>0){
            NotasGrafico.trocaTelaAvaliacoes("Menu.fxml", vetAluno,aluno,avaliacoes);
        }
        else{
            NotasGrafico.trocaTelaArray("Menu.fxml", avaliacoes,-1);
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
    }    

    
}
