package notasgrafico;

import java.util.Scanner;

public class Aluno {
    String nome;
    double[] notas1;
    double[] notas2;
    Scanner ler;

    public Aluno (String nome, double tri1,double tri2, double tri3){
        notas1 = new double [3];
        notas2 = new double [3];
        this.nome = nome;
        this.notas1 [0] = tri1;
        this.notas1 [1] = tri2;
        this.notas1 [2] = tri3;
        for (int x=0; x<3;x++){
            this.notas2[x] = -1;
        }

    }

    public void notaTrimestre (int tri, double notaLida){
        if (notaLida==0){
            notaLida=0.1;
        }
        this.notas2[tri] = notaLida;

}
}
