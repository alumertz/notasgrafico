package notasgrafico;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;


public class CadastrarAlunoController implements Initializable {
    @FXML
    TextField tri1,tri2,tri3,nome;
    ArrayList<Aluno> vetAluno;
    ArrayList<Double> avaliacoes;
    int aluno;
    @FXML
    public void cadastrar(){
        vetAluno.add(new Aluno(nome.getText(), Double.parseDouble(tri1.getText()), Double.parseDouble(tri2.getText()), Double.parseDouble(tri3.getText())));
        NotasGrafico.trocaTelaAvaliacoes("Menu.fxml", vetAluno,vetAluno.size(),avaliacoes);
    }
    public void setVetAlunos (ArrayList<Aluno> vetAlunos){
        vetAluno = vetAlunos;
    }
    public void setAvaliacoes(ArrayList<Double> vetAvaliacoes){
        avaliacoes = vetAvaliacoes;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }   
    
}
