package notasgrafico;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;


public class MenuController implements Initializable {
    ArrayList<Double> pesos = new ArrayList<>();
    ArrayList<Aluno> vetAluno = new ArrayList<>();
    int aluno;
    
    @FXML
    private TextField nTrimestre,nAluno;
    @FXML
    private AnchorPane tela;
    @FXML
    private Label lAluno,aviso, nomeAluno;
    @FXML
    private Label trim, trim1,trim2, trim3, prog1,prog2,nota11, nota12,nota13,nota21,nota22,nota23;
        
    @FXML
    private void cadastrar() {
        
        if (vetAluno.isEmpty()){
            NotasGrafico.trocaTelaAvaliacoes("CadastrarAluno.fxml", vetAluno, -1, pesos );
        }
        else{
        
           NotasGrafico.trocaTelaAvaliacoes("CadastrarAluno.fxml", vetAluno, Integer.parseInt (lAluno.getText()),pesos ); 
        }
    }
    @FXML
    private void calcularMedia(){
        
        if (vetAluno.isEmpty()){
            NotasGrafico.trocaTelaArray("CalcularMedia.fxml",pesos, -1);
        }
        else{
            NotasGrafico.trocaTelaAvaliacoes("CalcularMedia.fxml",vetAluno, Integer.parseInt (lAluno.getText()), pesos);
        }
    }
    public void setAvaliacoes(ArrayList<Double> avaliacoes){
        pesos = avaliacoes;
    }
    public void setVetAlunos (ArrayList<Aluno> vetAlunos, int alunoManipulado){
        vetAluno = vetAlunos;
        aluno = alunoManipulado;
        nomeAluno.setText(vetAlunos.get(aluno-1).nome);
        lAluno.setText(Integer.toString(aluno));
        
    }
    @FXML
    private void mostrarHistorico(){
        DecimalFormat df = new DecimalFormat("#.0");
        if (vetAluno.isEmpty()){
            aviso.setText("Você não criou um aluno!");
        }
        else{
            trim.setText("Trim.");
            trim1.setText("1°");
            trim2.setText("2°");
            trim3.setText("3°");
            prog1.setText("Prog.1");
            prog2.setText("Prog.2");
            nota11.setText(df.format((vetAluno.get(aluno-1).notas1[0])));
            nota12.setText(df.format(vetAluno.get(aluno-1).notas1[1]));
            nota13.setText(df.format(vetAluno.get(aluno-1).notas1[2]));
            
            for (int x =0; x<3;x++){
                if (vetAluno.get(aluno-1).notas2[x]==-1){
                    switch (x){
                        case 0:
                            nota21.setText("Ø");
                            break;
                        case 1:
                            nota22.setText("Ø");
                            break;
                        case 2:
                            nota23.setText("Ø");
                            break;
                    }
                }
                else{
                    nota21.setText(df.format(vetAluno.get(aluno-1).notas2[0]));
                    nota22.setText(df.format(vetAluno.get(aluno-1).notas2[1]));
                    nota23.setText(df.format(vetAluno.get(aluno-1).notas2[2]));
                }
            }
            
            Line l1 = new Line(195,250,330, 250);
            Line l2 = new Line(195,280,330, 280);
            Line l3 = new Line(195,310,330, 310);
            Line l4 = new Line(240,230,240, 310);
            Line l5 = new Line(270,230,270, 310);
            Line l6 = new Line(300,230,300, 310);
            Line l7 = new Line(330,230,330, 310);
           
            this.tela.getChildren().addAll(l1,l2,l3,l4,l5,l6,l7);
        }
       
    }
    @FXML
    private void mostrarTurma(){
        DecimalFormat df = new DecimalFormat("#.0");
        int tri = Integer.parseInt(nTrimestre.getText());
        nTrimestre.setText("");
        aviso.setText("Relatório no terminal!");
        System.out.println ("N° - Aluno\t\t\tNota:");
        for (int x=0;x< vetAluno.size(); x++){
            if (vetAluno.get(x).notas2[tri-1]==-1){
                System.out.println ((x+1) +" - " + vetAluno.get(x).nome + "\t\t\t Ø");
            }
            else{
                System.out.println ((x+1) +" - " + vetAluno.get(x).nome + "\t\t\t" + df.format(vetAluno.get(x).notas2[tri-1]));
            }
        }
    }
    @FXML
    private void trocarAluno(){
        aluno = Integer.parseInt(nAluno.getText());
        lAluno.setText(Integer.toString(aluno));
        nomeAluno.setText(vetAluno.get(aluno-1).nome);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
